const express =  require("express")
const cors =  require("cors")
const { application } = require("express")

const app = express()



app.use(cors({
    origin: '*'
}));

app.use(express.json())

app.use(express.urlencoded({extended:true}))

app.get("/", (req, res) => {
    res.json({message: "welcome to my node  app"})
})

require("./app/routes/tutorial.routes.js")(app)

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})